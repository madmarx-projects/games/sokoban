C=gcc
LD=-L /usr/bin/ld
EXEC=sokoban

sokoban: sokoban.o fonctions.o fonctions.h
	$(CC) $^ -o $@

.PHONY: clean mrproper

clean:
	@rm -rf *.o

mrproper: clean
	@rm -rf $(EXEC)
