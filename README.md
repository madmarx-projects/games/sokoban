# Sokoban

Ce projet est un sokoban conçu en C, se jouant dans le terminal.

## Illustration

![Sokoban](https://i.imgur.com/nk8oaup.png)

## Compile

``` bash
make
```

## Run

``` bash
./sokoban
```

