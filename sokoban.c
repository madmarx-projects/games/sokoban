/*
  ╔═══════════════════════════════╗
  ║  Auteur:  Valentin MARX       ║
  ║  Groupe:  B                   ║
  ║  Sujet:   C                   ║
  ║  Date:    20/05/15            ║
  ║  File:    sokoban.c           ║
  ╚═══════════════════════════════╝
  */

#include "fonctions.h"

int main(int argc, char const *argv[]) {

  int nbSoko = compterSokoban();

  //quitter le programme si le fichier est absent ou qu'il ne contient aucun niveau
  if (nbSoko == 0) {
    perror("Aucun niveau à charger !\nEXIT\n\n");
    return 1;
  }

  char* pseudo = malloc(20);
  identification(pseudo);

  int niveauMax = 0;
  chargementPseudo(pseudo, &niveauMax);

  joueur j;
  j.nom = malloc(20 * sizeof(char));
  strcpy(j.nom, pseudo);

  j.levelMax = niveauMax;

  sokoban sok;
  sok.player = j;

  if (sok.player.levelMax > 1) {
    affichageMenu();
    choixMenu(&sok.level, &sok.player, nbSoko);
  } else {
    sok.level = 1;
  }

  while(sok.level != -1) {
    int reessayer = 1, continuer = 1;
    while(reessayer == 1 | continuer == 1) {

      int lig, col;
      continuer = 0;

      sok.entrep.nbDeplJoueur = 0;
      sok.entrep.nbCaisseDepl = 0;

      remplissageGrille(&lig, &col, sok.level, &(sok.entrep));

      int fini = 0;
      char commande = 0;
      while(fini == 0 && commande != 'x') {

        affichageNiveau(lig, col, sok.entrep.tab);
        printf("\t\t%d | %d\n", sok.entrep.nbDeplJoueur, sok.entrep.nbCaisseDepl);

        commande = entreeCommande();

        if (commande != 'x') {

          deplacement(&sok.entrep, commande);

          if (sok.entrep.nbPalette == 0) {
            fini = 1;

            sauvegardePartie(sok.player.nom, sok.level, sok.entrep.nbDeplJoueur, sok.entrep.nbCaisseDepl);

            affichageNiveau(lig, col, sok.entrep.tab);
            printf("\t\t%d | %d\n\n", sok.entrep.nbDeplJoueur, sok.entrep.nbCaisseDepl);
            printf("\n\t Niveau réussi ! \n\n");

            if (sok.player.levelMax < nbSoko & sok.level == sok.player.levelMax) {
              sok.player.levelMax++;
              printf("Vous venez de débloquer le niveau %d !\n", sok.player.levelMax);
            }

            printf("\n  Record personnel : %d\n", meilleurScorePerso(sok.player.nom, sok.level));
            printf(  "    Record mondial : %d\n", meilleurScoreGlobal(sok.level));
            printf("\n\n\n\t   ");
            couleur("4");  //souligné
            printf("TOP 10");
            couleur("0");
            printf("\n");
            affichageScoresNiveau(sok.level);

            if (sok.level < nbSoko) {
              continuer = entreeContinuer();
              if (continuer == 1) {
                sok.level++;
              } else {
                reessayer = 0;
              }
            } else {
              couleur("5");
              printf("\n FÉLICITATIONS VOUS AVEZ TERMINÉ TOUS LES NIVEAUX !");
              couleur("0");
              getchar();
              puts("\n\n");
              reessayer = 0;
            }
          }
        } else {
          reessayer = entreeReessayer();
        }
      }

      //FREE Uniquement si rempli une fois
      int i = 0;
      for (i=0; i<lig; ++i)
        free(sok.entrep.tab[i]);
      free(sok.entrep.tab);
    }
    do{
      affichageMenu();
      choixMenu(&sok.level, &sok.player, nbSoko);
    } while(sok.level == 0);
  }

  //sauvegarde du joueur
  sauvegardeJoueur(sok.player);

  puts("\n");

  return 0;
}

