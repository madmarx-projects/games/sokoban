/*
   ╔═══════════════════════════════╗
   ║  Auteur:  Valentin MARX       ║
   ║  Groupe:  B                   ║
   ║  Sujet:   C                   ║
   ║  Date:    20/05/15            ║
   ║  File:    fonctions.c         ║
   ╚═══════════════════════════════╝
   */

#include "fonctions.h"

//vider le buffer
void vider_stdin(void) {
  scanf("%*[^\n]");
  getchar();
}

/*  Pré:  /
 *  Post: /
 *  Rés:  compte le nombre de '+' ou '@', qui correspond au nombre de gardien
 *          et donc au nombre de niveau(x)
 *        si pas de fichier -> 0
 *        si rien dans le fichier -> 0
 *        sinon -> le nombre de niveau
 */
//compte le nombre de niveaux dans le fichier
int compterSokoban() {
  int res = 0;
  char temp = 0;

  FILE* fichier;

  if ((fichier = fopen("format_texte/levels/skbAll.txt", "r")) != NULL) {
    while (temp != EOF) {
      temp = fgetc(fichier);
      if (temp == '+' | temp == '@')
        res++;
    }
  } else {
    perror(" Erreur de lecture ");
  }

  return res;
}

/*  Pré:  pseudo est initialisé et peut contenir 20 caractères
 *  Post: pseudo est modifié selon ce que l'utilisateur entre
 */
//identification du joueur
void identification(char* pseudo) {
  printf("\nEntrez votre pseudo (20 caractères max):  ");
  scanf("%20s", pseudo);
  vider_stdin();
}

// choisir que faire pour le lancement de la partie
void affichageMenu() {
  puts("\f\n\tContinuer la partie...........1");
  puts(    "\tChoisir le niveau.............2");
  puts(    "\tAfficher les scores...........3");

  puts(  "\n\tQuitter.......................0");
}

/*  Pré:  niveau, joueur et nbSoko sont initialisés
 *        nbSoko >= 1
 *        1 <= joueur.levelMax <= nbSoko
 *  Post: niveau est modifié, joueur et nbSoko sont inchangés
 *  Rés:  si choix = 1, niveau = j.levelMax
 *        sinon si choix = 2,  1 <= niveau <= j.levelMax
 *        sinon si choix = 3, niveau = 0
 *        sinon niveau = -1
 */
//niveau == -1 si affichage du score ou désir de quitter 
void choixMenu(int* niveau, joueur* j, int nbSoko) {

  int choix = -1;
  do {
    printf("\nQue voulez-vous faire?  ");
    scanf("%d", &choix);
    vider_stdin();
  } while (choix != 1 & choix != 2 & choix != 3 & choix != 0);

  switch(choix) {
    case 1:
      *niveau = j->levelMax;
      break;

    case 2:
      printf("\n");
      int temp = 0;
      printf("Quel niveau souhaitez-vous jouer (1-%d)?  ", j->levelMax);
      scanf("%d", &temp);
      vider_stdin();
      while (temp <= 0 | temp > j->levelMax) {
        printf("Introduisez un nombre entre 1 et %d svp :  ", j->levelMax);  
        scanf("%d", &temp);
        vider_stdin();
      }
      *niveau = temp;
      break;

    case 3:
      printf("\n");
      int nivAff = 0;
      printf("Quel niveau souhaitez-vous afficher (1-%d)?  ", nbSoko);
      scanf("%d", &nivAff);
      vider_stdin();
      while (nivAff <= 0 | nivAff > nbSoko) {
        printf("Introduisez un nombre entre 1 et %d svp :  ", nbSoko);  
        scanf("%d", &nivAff);
        vider_stdin();
      }
      affichageScoresNiveau(nivAff);
      getchar();
      *niveau = 0;
      break;

    case 0:
      *niveau = -1;
      printf("\nEXIT\n");
      break;

    default:
      printf("\nErreur !\n");
      break;
  }
}

/*  Pré:  pseudo et niveau sont initialisés
 *        niveau >= 1
 *  Post: pseudo est inchangé
 *        Si c'est un nouveau joueur, l'ajouter dans le fichier
 *        Sinon le charger simplement
 */
void chargementPseudo(char* pseudo, int* niveau) {
  int state = chercherJoueur(pseudo, niveau);

  if (state == 0) {
    nouveauJoueur(pseudo);
  }
}

/*  Pré:  pseudo et niveau sont initialisés
 *        niveau >= 1
 *  Post: pseudo est inchangé
 *  Rés:  si pseudo trouvé, res = 1
 *        sinon res = 0 et niveau = 1
 */
int chercherJoueur(char* pseudo, int* niveau) {
  FILE* fichier;

  *niveau = 1;
  int res = 0;
  int trouve = 0;
  char temp[20];

  if ((fichier = fopen(FICH_TXT_PLAYERS, "r")) != NULL) {
    while (trouve == 0 && fgetc(fichier) != EOF) {
      fseek(fichier, -1, 1);
      fscanf(fichier, "%s[^ ]", temp);
      if (strcmp(temp, pseudo) == 0) {
        fscanf(fichier, "%d", niveau);
        trouve = 1;
        res = 1;
      }
    }
    fclose(fichier);
  } else if ((fichier = fopen(FICH_TXT_PLAYERS, "w")) != NULL) {
    printf("Création du fichier en cours...\n");
    fclose(fichier);
  } else {
    perror(" Erreur de lecture/écriture ");
  }

  return res;
}

/*  Pré:  pseudo est initialisé
 *  Post: pseudo est inchangé
 *        à la suite dans le fichier "[pseudo] 1"
 */
void nouveauJoueur(char* pseudo) {
  FILE* fichier;

  if ((fichier = fopen(FICH_TXT_PLAYERS, "a+")) != NULL) {
    printf("Ajout de %s au bout dans la base de donnée...", pseudo);
    fprintf(fichier, "%s 1\n", pseudo);
    fclose(fichier);
  } else {
    perror(" Erreur d'écriture ");
  }

}

/*  Rés:  commande = 'z', 'q', 's', 'd' ou 'x'
*/
char entreeCommande() {
  char commande = 0;

  printf("\nDirection (z-q-s-d, x pour quitter) : ");
  scanf("%c", &commande);
  vider_stdin();
  while (commande != 'z' & commande != 'q' & commande != 's' & commande != 'd' & commande != 'x') {
    printf("\nEntrez une direction valide (z-q-s-d, x pour quitter) : ");
    scanf("%c", &commande);
    vider_stdin();
  }

  return commande;
}

/*  Rés:  si veut continuer, res = 1
 *        sinon res = 0
 */
int entreeContinuer() {
  int res = 0;
  char continuer = 0;

  printf("\n\n\tContinuer (o-n)? ");
  scanf("%c", &continuer);
  vider_stdin();
  while (continuer != 'o' && continuer != 'n') {
    printf("Veillez à entrer 'o' ou 'n'\n\n\tContinuer? ");
    scanf("%c", &continuer);
    vider_stdin();
  }

  if (continuer == 'o' | continuer == 'O') {
    res = 1;
  } else if (continuer == 'n' | continuer == 'N') {
    res = 0;
  }

  return res;
}

/*  Rés:  si veut réessayer, res = 1
 *      sinon res = 0
 */
int entreeReessayer() {
  int res = 0;
  char reessayer = 0;

  printf("\n\n\tRéessayer (o-n)? ");
  scanf("%c", &reessayer);
  vider_stdin();
  while (reessayer != 'o' && reessayer != 'n') {
    printf("Veillez à entrer 'o' ou 'n'\n\n\tRéessayer? ");
    scanf("%c", &reessayer);
    vider_stdin();
  }

  if (reessayer == 'o' | reessayer == 'O') {
    res = 1;
  } else if (reessayer == 'n' | reessayer == 'N') {
    res = 0;
  }

  return res;
}

/*  Pré:   entrep et dir sont initialisés
 *      dir = 'z', 'q', 's' ou 'd'
 *  Post:   entrep et dir sont inchangés
 *      si peut avancer la direction choisie,
 *        avance dans la direction souhaitée
 *      sinon,
 *        avertissement
 */
void deplacement(entrepot* entrep, char dir) {
  int offsetY = 0;  //lignes
  int offsetX = 0;  //colonnes

  switch(dir) {
    //up
    case 'z' :
      offsetY = -1;
      break;

      //down
    case 's' :
      offsetY = 1;
      break;

      //left
    case 'q' :
      offsetX = -1;
      break;

      //right
    case 'd' :
      offsetX = 1;
      break;

    default :
      perror(" Erreur : input déplacement");
      break;
  }

  if (peutAvancer(entrep, offsetY, offsetX) == 0) {
    avancer(entrep, offsetY, offsetX);
  } else {
    printf(" Vous ne pouvez pas avancer dans cette direction...");
  }

}


/*Pré:  entrep, x et y sont initialisés
 *      y correspond au décalage de la ligne (-1, 0 ou 1) (déplacement sur la colonne)
 *      x correspond au décalage de la colonne (-1, 0 ou 1) (déplacement sur la ligne)
 *Post: entrep, x et y sont inchangés
 *Rés:  si la case suivante est vide, res = 0
 *      si la case suivante est une caisse et que la case d'après est vide, res = 0
 *      si la case suivante est un mur, res = -1
 *      si la case suivante est une caisse et que la case d'après est une caisse ou mur, res = -1
 */
int peutAvancer(entrepot* entrep, int y, int x) {
  int res = 0;

  if (entrep->tab[entrep->guard.lig + y][entrep->guard.col + x] == '#') {
    res = -1;
  } else if (entrep->tab[entrep->guard.lig + y][entrep->guard.col + x] == '$' | entrep->tab[entrep->guard.lig + y][entrep->guard.col + x] == '*' &&
      (entrep->tab[entrep->guard.lig + y*2][entrep->guard.col + x*2] == '$' | entrep->tab[entrep->guard.lig + y*2][entrep->guard.col + x*2] == '*' |
       entrep->tab[entrep->guard.lig + y*2][entrep->guard.col + x*2] == '#')) {
    res = -1;
  }

  return res;
}

/*Pré:  entrep, x et y sont initialisés
 *      y correspond au décalage de la ligne (-1, 0 ou 1) (déplacement sur la colonne)
 *      x correspond au décalage de la colonne (-1, 0 ou 1) (déplacement sur la ligne)
 *Post: entrep est modifié, x et y sont inchangés
 *      si entrep.tab_0[guard->lig][guard->col] == '@'
 *        --> entrep.tab[guard->lig][guard->col] = '_'
 *      sinon si entrep.tab_0[guard->lig][guard->col] == '+'
 *        --> entrep.tab[guard->lig][guard->col] = '.'
 *
 *      si entrep.tab_0[guard->lig + y][guard->col + x] == '_' OU entrep.tab_0[guard->lig + y][guard->col + x] == '$'
 *        entrep.tab[guard->lig + y][guard->col + x] = '@'
 *      sinon si entrep.tab_0[guard->lig + y][guard->col + x] == '.' OU entrep.tab_0[guard->lig + y][guard->col + x] == '*'
 *        entrep.tab[guard->lig + y][guard->col + x] = '+'
 *
 *      si entrep.tab_0[guard->lig + y][guard->col + x] == '$' OU entrep.tab_0[guard->lig + y][guard->col + x] == '*'
 *        si entrep.tab_0[guard->lig + y*2][guard->col + x*2] == '_'
 *          entrep.tab[guard->lig + y*2][guard->col + x*2] = '$'
 *        sinon si entrep.tab_0[guard->lig + y*2][guard->col + x*2] == '.'
 *          entrep.tab[guard->lig + y*2][guard->col + x*2] = '*'
 *
 *      Si une caisse est déplacée, entrep.nbCaisseDepl++
 *      entrep.nbDeplJoueur++
 *      guard_0->lig += y, guard_0->col += x
 *
 */
void avancer(entrepot* entrep, int y, int x) {
  char tempCase1 = entrep->tab[entrep->guard.lig][entrep->guard.col];
  char tempCase2 = entrep->tab[entrep->guard.lig + y][entrep->guard.col + x];
  char tempCase3 = 0;
  if (entrep->tab[entrep->guard.lig + y][entrep->guard.col + x] == '$' | entrep->tab[entrep->guard.lig + y][entrep->guard.col + x] == '*') {
    tempCase3 = entrep->tab[entrep->guard.lig + y*2][entrep->guard.col + x*2];
  }

  if (tempCase1 == '@') {
    entrep->tab[entrep->guard.lig][entrep->guard.col] = '_';
  } else if (tempCase1 == '+') {
    entrep->tab[entrep->guard.lig][entrep->guard.col] = '.';
  }

  if (tempCase2 == '_' | tempCase2 == '$') {
    entrep->tab[entrep->guard.lig + y][entrep->guard.col + x] = '@';
  } else if (tempCase2 == '.' | tempCase2 == '*') {
    entrep->tab[entrep->guard.lig + y][entrep->guard.col + x] = '+';
    if (tempCase2 == '*') {
      entrep->nbPalette++;
    }
  }

  //si passe dans ce IF, c'est qu'un caisse à été déplacée
  if (tempCase3 != 0) {
    if (tempCase3 == '_') {
      entrep->tab[entrep->guard.lig + y*2][entrep->guard.col + x*2] = '$';
    } else if (tempCase3 == '.') {
      entrep->tab[entrep->guard.lig + y*2][entrep->guard.col + x*2] = '*';
      entrep->nbPalette--;
    }
    entrep->nbCaisseDepl++;
  }

  entrep->nbDeplJoueur++;

  entrep->guard.lig += y, entrep->guard.col += x;
}

/*  Pré:  lig, col et tab sont initialisés
 *        1 <= lig <= 18
 *        1 <= col <= 31
 *  Post: lig, col et tab sont inchangés
 *  Rés:  le plateau de jeu est affiché avec des couleurs spécifiques
 *        à chaque type de case
 */
void affichageNiveau(int lig, int col, char** tab) {
  system("clear");
  puts("\n");

  int i;
  for (i=0; i<lig; ++i) {
    int j;
    printf("\t");
    for (j=0; j<col; ++j) {
      switch (tab[i][j]) {
        case '#':
          couleur("47");
          couleur("37");
          printf("##");
          break;
        case '_':
          couleur("0");
          printf("  ");
          break;
        case '@':
          couleur("05");
          couleur("45");
          printf("II");
          break;
        case '+':
          couleur("05");
          couleur("47");
          couleur("35");
          printf("II");
          break;
        case '.':
          couleur("40");
          couleur("32");
          printf("--");
          break;
        case '$':
          couleur("42");
          couleur("30");
          printf("--");
          break;
        case '*':
          couleur("42");
          couleur("37");
          printf("  ");
          break;
        default:
          printf("Erreur de reconnaissance de caractère\n");
          break;
      }
      couleur("0");
    }
    printf("\n");
  }
  printf("\n");

}


/*  Pré:  lign, colo, niveau et entrep sont initialisés
 *  Post: lign et colo sont changés
 *        niveau et entrep sont inchangés
 *  Rés:  lign correspond au nombre de lignes du niveau
 *        colo correspond au nombre de colonnes du niveau
 */
int remplissageGrille(int* lign, int* colo, int niveau, entrepot* entrep) {
  FILE* fichier;

  char* path = malloc(30);
  strcpy(path, DOS_TXT_LVL);
  path = strcat(path, "skbAll.txt");

  if ((fichier = fopen(path, "r")) != NULL) {
    int cptNiveau = 0;
    int lig = 0, col = 0;

    while (cptNiveau < niveau) {
      fscanf(fichier, "%d %d", &col, &lig);
      if (lig > 0) {
        cptNiveau++; 
        if (cptNiveau != niveau) { 
          int temp = 0; 
          while (temp < lig+1) { 
            if (fgetc(fichier) == '\n') { 
              temp++; 
              printf("%d\n", temp);
            }
          }
        }
      }
    }

    *lign = lig;
    *colo = col;

    char** tab = malloc(lig * sizeof(char*));
    int k = 0;
    for (k=0; k<lig; ++k) {
      tab[k] = malloc(col * sizeof(char));
    }

    entrep->tab = tab;
    entrep->nbPalette = 0;

    while(fgetc(fichier) != '\n');
    int i=0, j=0;
    char temp;
    for (i=0; i<lig; ++i) {
      for (j=0; j<col; ++j) {

        entrep->tab[i][j] = fgetc(fichier);

        if (entrep->tab[i][j] == '@' | entrep->tab[i][j] == '+') {
          entrep->guard.lig = i;
          entrep->guard.col = j;
        } else if (entrep->tab[i][j] == '.') {
          entrep->nbPalette++;
        }
      }
      fgetc(fichier);  //CR
      // fgetc(fichier);  //LF
    }

    fclose(fichier);

    entrep->tab = tab;

  } else {
    perror(" Erreur de lecture ");
  }

}

/*  Pré:  pseudo et niveau sont initialisés
 *  Post: pseudo et niveau sont inchangés
 *  Rés:  si pas réussi à ouvrir le fichier ou si fichier vide, -1
 *        best = le plus petit score du fichier
 */
int meilleurScorePerso(char* pseudo, int niveau) {
  char* path = malloc(45);
  strcpy(path, DOS_TXT_PARTIES);
  path = strcat(path, pseudo);
  path = strcat(path, ".txt");

  FILE* fichier;
  if ((fichier = fopen(path, "r")) != NULL) {
    int best = 100000, score = 0, lvl;
    char temp = 0;
    do {
      fscanf(fichier, "%d", &lvl);
      if (lvl == niveau) {
        fscanf(fichier, "%d", &score);
        if (score > 0 & score < best) {
          best = score;
        }
      }
      temp = fgetc(fichier);
    } while(temp != EOF);
    if (best != 100000) {
      return best;
    } else {
      return -1;
    }
    fclose(fichier);
  } else {
    perror(" Erreur de lecture/écriture ");
  }

  free(path);

  return -1;
}

/*  Pré:  niveau est initialisé
 *        niveau >= 1
 *  Post: niveau est inchangé
 *  Rés:  si pas réussi à ouvrir le fichier ou si fichier vide, -1
 *        best = le plus petit score du fichier
 */
int meilleurScoreGlobal(int niveau) {

  char* path = malloc(45);
  char* str = malloc(2);
  sprintf(str, "%d", niveau);

  strcpy(path, DOS_TXT_LVL);
  path = strcat(path, str);
  path = strcat(path, ".txt");

  FILE* fichier;
  if ((fichier = fopen(path, "r")) != NULL) {
    int best = 100000, score = 0, dummy = 0;
    char temp = 0;
    do {
      fscanf(fichier, "%d %d", &score, &dummy);
      if (score > 0 & score < best) {
        best = score;
      }
      temp = fgetc(fichier);
    } while (temp != EOF);
    if (best != 100000) {
      return best;
    } else {
      return -1;
    }
    fclose(fichier);
  } else {
    perror(" Erreur de lecture ");
  }

  free(path);

  return -1;
}

/*  Pré:  niveau est initialisé
 *        niveau >= 1
 *  Post: niveau est inchangé
 *  Rés:  affichage de tous les pseudos et scores des joueurs
 *        ayant joué ce niveau
 */
void affichageScoresNiveau(int niveau) {
  char* path = malloc(45);
  char* pseudo = malloc(20);
  char* str = malloc(2);
  sprintf(str, "%d", niveau);
  strcpy(path, DOS_TXT_LVL);
  path = strcat(path, str);
  path = strcat(path, ".txt");

  FILE* fichier;

  if ((fichier = fopen(path, "r")) != NULL) {
    int nbLignes = -1;
    char temp = 0;
    while (temp != EOF) {
      temp = fgetc(fichier);
      if (temp == '\n') {
        nbLignes++;
      }
    }
    rewind(fichier);

    int* check = malloc(nbLignes * sizeof(int));
    int i;
    for (i=0; i<nbLignes; ++i) {
      check[i] = 0;    //pas encore checké
    }

    int score = 0, dummy = 0;
    int cpt = 0;
    while (cpt < 10 && cpt < nbLignes) {
      rewind(fichier);

      temp = 0;
      int indice, k = 0;

      int best = 100000;
      char* pseudoBest = malloc(20);

      do {
        fscanf(fichier, "%d %d %s[^ ]", &score, &dummy, pseudo);
        if (score > 0 && (check[k] == 0 && score < best)) {
          best = score;
          strcpy(pseudoBest, pseudo);
          indice = k;
        }
        temp = fgetc(fichier);
        k++;
      } while (temp != EOF);

      printf("\t%s\t%d\n", pseudoBest, best);
      //coché (n'est plus à checker)
      check[indice] = 1;

      free(pseudoBest);

      cpt++;
    }

    fclose(fichier);
  } else {
    perror(" PAS ENCORE DE SCORES POUR CE NIVEAU !  ");
  }

  free(path);
}

/*  Pré:  player est initialisé
 *  Post: player est inchangé
 *  Rés:  le joueur.levelMax est mis à jour dans le fichier joueurs.txt
 */
void sauvegardeJoueur(joueur player) {
  FILE * fichier;

  char* path = malloc(45);
  strcpy(path, FICH_TXT_PLAYERS);
  char temp[20];

  if (fichier = fopen(path, "r+")) {
    int trouve = 0;
    while (trouve == 0 && fgetc(fichier) != EOF) {
      fseek(fichier, -1, 1);
      fscanf(fichier, "%s[^ ]", temp);
      if (strcmp(temp, player.nom) == 0) {
        fseek(fichier, 1, 1);
        fprintf(fichier, "%d", player.levelMax);
        trouve = 1;
      }
    }
    fclose(fichier);
  } else {
    perror(" Erreur de lecture ");
  }

  free(path);
}

/*  Pré:  pseudo, niveau, nbDeplJ et nbDeplC sont initialisés
 *        niveau >= 1, nbDeplJ >= 1, nbDeplC >= 0
 *  Post: pseudo, niveau, nbDeplJ et nbDeplC sont inchangés
 *  Rés:  le résultat de la partie est ajouté
 *        à la fin du fichier [niveau].txt
 *        à la fin du fichier [pseudo].txt
 */
void sauvegardePartie(char* pseudo, int niveau, int nbDeplJ, int nbDeplC) {
  FILE * fichierJoueur;

  char* pathJ = malloc(45);
  strcpy(pathJ, DOS_TXT_PARTIES);
  pathJ = strcat(pathJ, pseudo);
  pathJ = strcat(pathJ, ".txt");

  if ((fichierJoueur = fopen(pathJ, "a+")) != NULL) {
    fprintf(fichierJoueur, "%d %d %d\n", niveau, nbDeplJ, nbDeplC);
    fclose(fichierJoueur);
  } else if ((fichierJoueur = fopen(pathJ, "w")) != NULL) {
    printf("Le fichier %s n'existe pas, création et ajout de la première partie...\n", pathJ);
    fprintf(fichierJoueur, "%d %d %d\n", niveau, nbDeplJ, nbDeplC);
    fclose(fichierJoueur);
  } else {
    perror(" Erreur de lecture/écriture ");
  }

  free(pathJ);

  FILE * fichierNiveau;

  char* path = malloc(45);
  char* str = malloc(2);
  sprintf(str, "%d", niveau);
  strcpy(path, DOS_TXT_LVL);
  path = strcat(path, str);
  path = strcat(path, ".txt");

  if ((fichierNiveau = fopen(path, "a+")) != NULL) {
    fprintf(fichierNiveau, "%d %d %s\n", nbDeplJ, nbDeplC, pseudo);
    fclose(fichierNiveau);
  } else if ((fichierNiveau = fopen(path, "w")) != NULL) {
    fprintf(fichierNiveau, "%d %d %s\n", nbDeplJ, nbDeplC, pseudo);
    fclose(fichierNiveau);
  } else {
    perror(" Erreur de lecture/écriture ");
  }

  free(path);
}
