/*
   ╔══════════════════════════════╗
   ║  Auteur:  Valentin MARX      ║
   ║  Groupe:  B                  ║
   ║  Sujet:   C                  ║
   ║  Date:    20/05/15           ║
   ║  File:    fonctions.h        ║
   ╚══════════════════════════════╝
   */

/*
   nbCol   correspond au nombre de colonnes de la grille du sokoban (largeur)
   nbLigne correspond au nombre de lignes de la grille du sokoban (hauteur)
   '#'     représente un mur
   '_'     représente une case vide de l’entrepôt
   '@'     représente le gardien sur un emplacement vide
   '+'     représente le gardien situé sur une palette
   '.'     représente une palette vide
   '$'     représente une caisse sur un emplacement vide
   '*'     représente une caisse sur une palette
   */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define couleur(param) printf("\033[%sm", param)

#define DOS_TXT_LVL "format_texte/levels/"
#define DOS_TXT_PARTIES "format_texte/parties/"
#define FICH_TXT_PLAYERS "format_texte/players/joueurs.txt"

#define DOS_BIN_LVL "format_binaire/levels/"
#define DOS_BIN_PARTIES "format_binaire/parties/"
#define FICH_BIN_PLAYERS "format_binaire/players/joueurs.bin"

typedef struct {
  int lig;
  int col;
} gardien;

typedef struct {
  char** tab;
  gardien guard;
  int nbPalette;
  int nbDeplJoueur;
  int nbCaisseDepl;
} entrepot;

typedef struct {
  char* nom;
  int levelMax;
} joueur;

typedef struct {
  int level;
  entrepot entrep;
  joueur player;
} sokoban;

//-------------------------------------------------//

#ifndef FONCTIONS_H
#define FONCTIONS_H
extern void vider_stdin(void);

extern int compterSokoban();

extern void identification(char* pseudo);
extern void affichageMenu();
extern void choixMenu(int* niveau, joueur* j, int nbSoko);
extern void chargementPseudo(char* pseudo, int* niveau);
extern int chercherJoueur(char* pseudo, int* niveau);
extern void nouveauJoueur(char* pseudo);

extern int remplissageGrille(int* lig, int* col, int niveau, entrepot* entrep);
extern void affichageNiveau(int lig, int col, char** tab);

extern char entreeCommande();
extern int entreeContinuer();
extern int entreeReessayer();

extern void deplacement(entrepot* entrep, char dir);
extern int peutAvancer(entrepot* entrep, int y, int x);
extern void avancer(entrepot* entrep, int y, int x);

extern int meilleurScorePerso(char* pseudo, int niveau);
extern int meilleurScoreGlobal(int niveau);
extern void affichageScoresNiveau(int niveau);

extern void sauvegardeJoueur(joueur player);
extern void sauvegardePartie(char* pseudo, int niveau, int nbDeplJ, int nbDeplC);

/*
  pré :   le fichier nomfichierTexte, s’il existe, est valide (càd qu’il respecte le
            format décrit ci-dessus) et est fermé
          i est initialisé et 1<= i
          sok est initialisé (*sok existe)
  post :  le fichier nomfichierTexte est inchangé et si il existe au moins i
            sokobans dans le fichier nomfichierTexte,*sok est initialisé avec les
            données du sokoban qui se trouve à la position i du fichier.
          On considère que le premier sokoban est au niveau 1.
          Le fichier est fermé.
  res :   1 si *sok a bien été initialisé, 0 sinon
*/
// extern int lireFichierTexte(int i, char* nomfichierTexte, sokoban* sok);

#endif
